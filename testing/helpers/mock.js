// @flow
import { first, cloneDeep } from 'lodash';
import data from '../data';
import { getVideosIds } from '../../src/reducers/videoReducer';
import { getUserAvatar, getUsername } from '../../src/reducers/userReducer';
import type { AppState } from '../../src/reducers';
import type { VideoReducerState } from '../../src/reducers/videoReducer';
import type { AuthReducerState } from '../../src/reducers/authReducer';
import type { HintReducerState } from '../../src/reducers/hintReducer';
import type { SearchReducerState } from '../../src/reducers/searchReducer';
import type { UserReducerState } from '../../src/reducers/userReducer';

export const getMockData = (pathToFile: string): any => {
  const fullPath = `/method/${pathToFile}`;
  if (!data[fullPath]) {
    throw new Error(`MockHelper: data not found ${fullPath}`);
  }

  return data[fullPath];
};

export class MockStore implements AppState {

  auth: AuthReducerState = {
    isAuthorized: true,
    v: '5.52',
    access_token: '918cb13b66eb5bcfdcd66ad4f7ee98a94fdb7b271d121f5ceab8d56b028c6a6ac2f49f1828252f79505e1',
    expires_in: 86400,
    user_id: 11673524
  };

  hint: HintReducerState = {
    message: 'Test Hint Message'
  };

  video: VideoReducerState;

  search: SearchReducerState;

  user: UserReducerState;

  constructor() {
    this.user = first(getMockData('users.get').response);
    this.search = getMockData('video.search').response;
    this.video = getMockData('video.get').response;
  }

  getVideosIds() {
    return getVideosIds(this.video);
  }

  getUserName() {
    return getUsername(this.user);
  }

  getUserAvatar() {
    return getUserAvatar(this.user);
  }

  getFirstVideoItem() {
    return first(this.video.items);
  }

  cloneVideoList() {
    return cloneDeep(this.video.items);
  }
}
