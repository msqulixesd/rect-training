import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

export const findByKey = (component: Object, key: string): Object =>
  component.findWhere(node => (node.key() === key)
);

export const getContainerWrapper = (element, state) => {
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);
  const store = mockStore(state);
  return shallow(element, { context: { store } });
};