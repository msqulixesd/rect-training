import videoGet from './video.get';
import videoAdd from './video.add';
import videoEdit from './video.edit';
import videoDelete from './video.delete';
import videoSearch from './video.search';
import usersGet from './users.get';
import videoTrack from './videoTrack';

export default {
  '/method/video.get': videoGet,
  '/method/video.add': videoAdd,
  '/method/video.edit': videoEdit,
  '/method/video.delete': videoDelete,
  '/method/video.search': videoSearch,
  '/method/users.get': usersGet,
  '/method/videoTrack': videoTrack,
};
