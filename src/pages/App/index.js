// @flow
import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { getAuthUrl } from '../../services/oauthService';

import Login from '../Login';
import Player from '../../containers/PlayerContainer';

export type AppProps = {
	isAuthorize?: boolean
};

export class App extends Component {
  render() {
    const { isAuthorize } = this.props;

    const content = isAuthorize
			? <Player />
			: <Login authUrl={getAuthUrl()} />;

    return (
      <MuiThemeProvider>
        {content}
      </MuiThemeProvider>
    );
  }

  props: AppProps;
}

export default App;