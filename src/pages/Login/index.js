// @flow
import React, { Component } from 'react';
import Paper from 'material-ui/Paper/index';
import LockIcon from 'material-ui/svg-icons/action/lock';
import FlatButton from 'material-ui/FlatButton/index';

import style from './style.json';

type LoginProps = {
	authUrl: string
};

export class Login extends Component {

  render() {
    return (
      <div style={style.layout}>
        <Paper style={style.paper} zDepth={5}>
          <h1>Authentication required</h1>
          <FlatButton
            label="Login you account"
            labelPosition="after"
            primary
            icon={<LockIcon />}
            href={this.props.authUrl}
          />
        </Paper>
      </div>
    );
  }

  props: LoginProps;
}

export default Login;