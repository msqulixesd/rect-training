// @flow
import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton/index';
import MenuIcon from 'material-ui/svg-icons/action/view-headline';
import type { VideoTrackModel } from '../../types/VideoTrackModel';
import Header from '../../containers/components/HeaderContainer';
import GridPlaylist from '../../components/GridPlaylist';
import VideoPlayer from '../../components/VideoPlayer';
import Hint from '../../components/Hint';
import Search from '../../components/Search';
import VideoListComponent from '../../components/VideoList';

type PlayerState = {
	playingVideo: ?VideoTrackModel,
	isVideoContainerOpened: boolean
};

export type PlayerProps = {
  hintMessage?: ?string,
  videoCount?: number,
  userVideos?: Array<VideoTrackModel>,
  addedVideos?: Array<number>,
  searchCount?: number,
  searchResult?: Array<VideoTrackModel>,

  initialize?: Function,
  addVideo?: Function,
  editVideo?: Function,
  deleteVideo?: Function,
  searchVideos?: Function,
};

export class Player extends Component {

  state: PlayerState = {
    playingVideo: null,
    isVideoContainerOpened: true
  };

  componentWillMount() {
    const { initialize } = this.props;
    if (initialize) {
      initialize();
    }
  }

  render() {
    const { isVideoContainerOpened } = this.state;
    const {
      addVideo, editVideo, deleteVideo, searchVideos,
      hintMessage,
      videoCount, userVideos, addedVideos,
      searchCount, searchResult
    } = this.props;
    return (
      <div>
        <Header />

        <Search handleSearch={searchVideos}>
          <FlatButton
            key="menu"
            icon={<MenuIcon />}
            onClick={() => { this.setState({ isVideoContainerOpened: !isVideoContainerOpened }); }}
          />
        </Search>

        <GridPlaylist
          display-if={isVideoContainerOpened}
          items={userVideos}
          count={videoCount}
          updateItem={editVideo}
          deleteVideo={deleteVideo}
          handleOpenVideo={this.handleOpenVideo}
        />

        <VideoListComponent
          items={searchResult}
          count={searchCount}
          addedVideos={addedVideos}
          handleOpenVideo={this.handleOpenVideo}
          handleAddVideo={addVideo}
        />

        <VideoPlayer
          track={this.state.playingVideo}
          handleClose={this.handleClosePlayingVideo}
        />

        <Hint message={hintMessage} />
      </div>
    );
  }

  handleClosePlayingVideo = () => {
    this.setState({ playingVideo: null });
  };

  handleOpenVideo = (item: VideoTrackModel) => {
    this.setState({ playingVideo: item });
  };

  props: PlayerProps;

}

export default Player;