export const APP_ID = 3839697;

export const APP_SCOPE_AUDIO = 'audio';
export const APP_SCOPE_VIDEO = 'video';

export const API_HOST = 'https://localhost:3000/api';
export const AUTH_HOST = 'https://oauth.vk.com';
export const APP_WEB_URL = 'https://localhost:3000';

export const API_VERSION = '5.52';

export const AUTH_DISPLAY_TYPE_PAGE = 'page';
export const AUTH_DISPLAY_TYPE_POPUP = 'popup';
export const AUTH_DISPLAY_TYPE_MOBILE = 'mobile';

export const AUTH_TYPE = 'token';

export const AUTH_DEFAULT_REDIRECT_URL = 'https://oauth.vk.com/blank.html';