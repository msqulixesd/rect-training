// @flow
import { connect } from 'react-redux';
import Header from '../../components/Header';
import type { HeaderProps } from '../../components/Header';
import type { AppState } from '../../reducers';
import * as userActions from '../../actions/userActions';
import * as userGetters from '../../reducers/userReducer';

export const mapStateToProps = (state: AppState): HeaderProps => ({
  username: userGetters.getUsername(state.user),
  avatar: userGetters.getUserAvatar(state.user),
});

export const mapDispatchToProps = (dispatch: Function): HeaderProps => ({
  initialize: async () => dispatch(userActions.getUserInfo())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
