// @flow
import { connect } from 'react-redux';
import type { AppState } from '../reducers';
import * as hintGetters from '../reducers/hintReducer';
import * as videoGetters from '../reducers/videoReducer';
import * as searchGetters from '../reducers/searchReducer';
import * as videoActions from '../actions/videoActions';
import * as hintActions from '../actions/hintActions';
import * as searchActions from '../actions/searchActions';
import Player from '../pages/Player';
import type { PlayerProps } from '../pages/Player';
import type { VideoTrackModel } from '../types/VideoTrackModel';

export const mapStateToProps = (state: AppState): PlayerProps => ({
  hintMessage: hintGetters.getHintMessage(state.hint),
  videoCount: videoGetters.getVideoCount(state.video),
  userVideos: videoGetters.getVideoList(state.video),
  addedVideos: videoGetters.getVideosIds(state.video),
  searchCount: searchGetters.getVideoCount(state.search),
  searchResult: searchGetters.getVideoList(state.search),
});

export const mapDispatchToProps = (dispatch: Function): PlayerProps => ({
  initialize: async () => dispatch(videoActions.getVideoItemsList()),
  addVideo: async (item: VideoTrackModel) => {
    await dispatch(videoActions.addVideo(item));
    dispatch(hintActions.updateHintMessage(`Видеозапись "${item.title}" добавлена`));
  },
  editVideo: async (item: VideoTrackModel) => {
    await dispatch(videoActions.updateVideo(item));
    dispatch(hintActions.updateHintMessage(`Видеозапись "${item.title}" обновлена`));
  },
  deleteVideo: async (item: VideoTrackModel) => {
    await dispatch(videoActions.deleteVideo(item));
    dispatch(hintActions.updateHintMessage(`Видеозапись "${item.title}" удалена`));
  },
  searchVideos: async(query:string) => dispatch(searchActions.searchVideos(query))
});

export default connect(mapStateToProps, mapDispatchToProps)(Player);
