// @flow
import { connect } from 'react-redux';
import type { AppState } from '../reducers';
import { getIsAuthorized } from '../reducers/authReducer';
import App from '../pages/App';
import type { AppProps } from '../pages/App';

export const mapStateToProps = (state: AppState): AppProps => ({
  isAuthorize: getIsAuthorized(state.auth)
});

export const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(App);
