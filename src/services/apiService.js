// @flow
import { assign } from 'lodash';
import RestService from './restService';
import FileService from './fileService';
import type { AuthReducerState } from '../reducers/authReducer';
import { IDataProvider } from './IDataProvider';

import {
	API_HOST,
	API_VERSION,
} from '../config';

import type { ApiAuthorizeResponse } from '../types/ApiAuthorizeResponse';

declare var API_ENV: string;

let authObject: ?ApiAuthorizeResponse = null;

const restProvider = new RestService(API_HOST);
const fileProvider = new FileService();

export const MOCKUP_ENV = 'mockup';
export const SERVER_ENV = 'server';

export const DEFAULT_ENV = API_ENV || SERVER_ENV;

export const isMockEnv = (env: string) => (env === MOCKUP_ENV);

const createRequest = (params: any = {}): Object => {
  if (isMockEnv(DEFAULT_ENV)) {
    return {};
  }

  if (!authObject) {
    throw new Error('Auth required');
  }

  return {
    ...params,
    access_token: authObject.access_token,
    v: API_VERSION
  };
};

const getProvider = (env: string = DEFAULT_ENV): IDataProvider => (
  isMockEnv(env) ? fileProvider : restProvider
);

export const GET = (method: string, params: any): Promise<*> => (
  getProvider().GET(`/method/${method}`, createRequest(params))
);

export const POST = (method: string, params: any): Promise<*> => (
  getProvider().POST(`/method/${method}`, createRequest(), params)
);

export const configure = (authParams: ?ApiAuthorizeResponse = null): void => {
  authObject = authParams;
};

export const isAuthorized = (): boolean => (!!authObject && !!authObject.access_token);

export function getUserId() : number {
  return authObject ? authObject.user_id : 0;
}

export function getAuthState(): AuthReducerState {
  return assign({
    isAuthorized: isAuthorized(),
    v: API_VERSION
  }, authObject || {});
}