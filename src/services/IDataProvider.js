// @flow

export interface IDataProvider {

  GET(path: string, params: ?any): Promise<*>;

  POST(path: string, request: ?any, params: ?any): Promise<*>;

}