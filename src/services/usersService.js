// @flow
import { first } from 'lodash';
import * as apiService from './apiService';
import * as nameCase from '../constants/nameCase';
import type { UserModel } from '../types/UserModel';
import type { ApiResponse } from '../types/ApiResponse';

export type NameCase = $Keys<nameCase>;

type GetUserModelRequest = {
	fields: string,
	name_case: NameCase
}

const METHOD_GET = 'users.get';

export const get = async (): Promise<UserModel> => {
  const params: GetUserModelRequest = {
    fields: 'first_name,last_name,photo_50',
    name_case: nameCase.GEN
  };
  const data: ApiResponse<Array<UserModel>> = await apiService.GET(METHOD_GET, params);
  return first(data.response);
};