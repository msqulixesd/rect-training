// @flow
import { chain, split } from 'lodash';
import buildUrl from 'build-url';
import RestService from './restService';
import {
	AUTH_HOST,
	APP_ID,
	AUTH_DISPLAY_TYPE_PAGE,
	APP_WEB_URL,
	APP_SCOPE_VIDEO,
	AUTH_TYPE,
	API_VERSION,
} from '../config';

import type { ApiAuthorizeRequest } from '../types/ApiAuthorizeRequest';
import type { ApiAuthorizeResponse } from '../types/ApiAuthorizeResponse';

const rest = new RestService(AUTH_HOST);

const authRequest: ApiAuthorizeRequest = {
  client_id: APP_ID,
  display: AUTH_DISPLAY_TYPE_PAGE,
  redirect_uri: APP_WEB_URL,
  scope: APP_SCOPE_VIDEO,
  response_type: AUTH_TYPE,
  v: API_VERSION,
  revoke: 1
};

export const authorize = (): Promise<*> => rest.GET('/authorize', authRequest);

export const getAuthUrl = (): string => buildUrl(rest.getPathUrl('/authorize'), {
  queryParams: authRequest
});

export const parseOauthResponse = (vkAuthHash: string): ApiAuthorizeResponse => {
  const response = chain(vkAuthHash)
		.trim('#')
		.split('&')
		.map(item => split(item, '='))
		.fromPairs()
		.value();

  return {
    access_token: response.access_token,
    expires_in: Number(response.expires_in),
    user_id: Number(response.user_id),
  };
};