// @flow
import { IDataProvider } from './IDataProvider';

class FileService implements IDataProvider {

  data: Object;

  constructor() {
    // eslint-disable-next-line import/no-dynamic-require, global-require
    this.data = require('../../testing/data').default;
  }

  GET(path: string): Promise<*> {
    return this.createResponse(this.getFile(path));
  }

  POST(path: string): Promise<*> {
    return this.createResponse(this.getFile(path));
  }

  getFile(path: string): any {
    if (!this.data[path]) {
      throw new Error(`FileService: Data not found on path ${path}`);
    }

    return this.data[path];
  }

  createResponse(data: any): Promise<any> {
    return new Promise(resolve => resolve(data));
  }

}

export default FileService;