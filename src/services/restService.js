// @flow
import rest from 'rest';
import { trim } from 'lodash';
import mime from 'rest/interceptor/mime';
import pathPrefix from 'rest/interceptor/pathPrefix';
import params from 'rest/interceptor/params';
import { IDataProvider } from './IDataProvider';

class RestService implements IDataProvider {

  apiHost: string;

  constructor(apiHost:string) {
    this.apiHost = apiHost;
  }

  GET(path: string, entity: any): Promise<*> {
    const client = rest.wrap(mime, { mime: 'application/json' })
			.wrap(pathPrefix, { prefix: this.apiHost })
			.wrap(params, { params: entity });

    return client({ path, method: 'GET' }).then(response => response.entity);
  }

  POST(path: string, urlParams: any, entity: any): Promise<*> {
    const client = rest.wrap(mime, { mime: 'application/json' })
			.wrap(pathPrefix, { prefix: this.apiHost })
			.wrap(params, { params: urlParams });

    return client({ path, method: 'POST', entity }).then(response => response.entity);
  }

  getPathUrl(path: string): string {
    return `${trim(this.apiHost)}/${trim(path)}`;
  }

}

export default RestService;