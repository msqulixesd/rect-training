// @flow
import * as apiService from './apiService';
import type { VideoList, VideoTrackModel } from '../types/VideoTrackModel';
import type { ApiResponse } from '../types/ApiResponse';

type GetUserVideosRequest = {
	owner_id?: ?number,
	videos?: ?string,
	album_id?: ?number,
	count?: ?number,
	offset?: ?number,
	extended?: 0 | 1,
}

type EditUserVideoRequest = {
	owner_id: number,
	video_id: number,
	name: string,
	desc: string
};

type DeleteUserVideoRequest = {
	video_id: number,
	owner_id: number,
	target_id?: number,
};

type SearchVideoRequest = {
	q: string,
	sort? : 0 | 1 | 2,
	hd?: number,
	adult?: 0 | 1,
	filters?: string,
	search_own?: 1 | 0,
	offset?: number,
	longer?: number,
	shorter?: number,
	count?: number,
	extended?: 1 | 0
};

type AddVideoToUserRequest = {
	target_id: number,
	video_id: number,
	owner_id: number
};

const METHOD_GET = 'video.get';
const METHOD_EDIT = 'video.edit';
const METHOD_DELETE = 'video.delete';
const METHOD_SEARCH = 'video.search';
const METHOD_ADD = 'video.add';

export const get = async (albumId: ?number): Promise<VideoList> => {
  const params: GetUserVideosRequest = {
    album_id: albumId
  };
  const data: ApiResponse<VideoList> = await apiService.GET(METHOD_GET, params);
  return data.response;
};

export const edit = async (item: VideoTrackModel): Promise<*> => {
  const params: EditUserVideoRequest = {
    owner_id: apiService.getUserId(),
    video_id: item.id,
    name: item.title,
    desc: item.description
  };

  return apiService.GET(METHOD_EDIT, params);
};

export const remove = async (item: VideoTrackModel): Promise<*> => {
  const params: DeleteUserVideoRequest = {
    target_id: apiService.getUserId(),
    owner_id: item.owner_id,
    video_id: item.id,
  };

  return apiService.GET(METHOD_DELETE, params);
};

export const search = async (query: string): Promise<VideoList> => {
  const params: SearchVideoRequest = {
    q: query
  };

  const data: ApiResponse<VideoList> = await apiService.GET(METHOD_SEARCH, params);
  return data.response;
};

export const add = async (item: VideoTrackModel): Promise<*> => {
  const params: AddVideoToUserRequest = {
    target_id: apiService.getUserId(),
    video_id: item.id,
    owner_id: item.owner_id
  };

  return apiService.GET(METHOD_ADD, params);
};