// @flow
import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/index';
import type { VideoTrackModel } from '../../types/VideoTrackModel';
import style from './style.json';

type VideoPlayerProps = {
	track: ?VideoTrackModel,
	handleClose?: Function,
};

export class VideoPlayer extends Component {

  render() {
    const { handleClose, track } = this.props;
    return (
      <Dialog
        title={track && track.title}
        modal={false}
        open={Boolean(track)}
        onRequestClose={handleClose}
        autoDetectWindowHeight
        autoScrollBodyContent
      >
        <iframe style={style.player} src={track && track.player} />
      </Dialog>
    );
  }

  props: VideoPlayerProps;
}

export default VideoPlayer;