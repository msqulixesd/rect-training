// @flow
import React, { Component } from 'react';
import moment from 'moment';
import { ListItem } from 'material-ui/List/index';
import DoneIcon from 'material-ui/svg-icons/action/done';
import AddIcon from 'material-ui/svg-icons/content/add-circle-outline';
import Avatar from 'material-ui/Avatar/index';

import type { VideoTrackModel } from '../../types/VideoTrackModel';

type VideoListItemProps = {
	data: VideoTrackModel,
	isAdded?: boolean,
	handleOpenVideo: Function,
	handleAddVideo: Function,
};

export class VideoListItem extends Component {


  render() {
    const { data, isAdded } = this.props;
    return (
      <ListItem
        leftAvatar={this.getAvatar(data)}
        primaryText={data.title}
        secondaryText={this.getAddedDate(data)}
        rightIcon={this.getAddButton(data, Boolean(isAdded))}
      />
    );
  }

  getAvatar(data: VideoTrackModel) {
    return (<Avatar
      src={data.photo_130}
      onClick={() => { this.props.handleOpenVideo(data); }}
    />);
  }

  getAddedDate(data: VideoTrackModel) {
    const created = moment.unix(data.date);
    return created.format('DD.MM.YYYY');
  }

  getAddButton(data: VideoTrackModel, isAdded: boolean) {
    if (isAdded) {
      return (<DoneIcon />);
    }

    return <AddIcon onClick={() => { this.props.handleAddVideo(data); }} />;
  }

  props: VideoListItemProps;
}

export default VideoListItem;