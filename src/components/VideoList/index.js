// @flow
import { map, indexOf } from 'lodash';
import React, { Component } from 'react';
import { List } from 'material-ui/List/index';
import Subheader from 'material-ui/Subheader/index';

import type { VideoTrackModel } from '../../types/VideoTrackModel';

import VideoListItem from './VideoListItem';

import style from './style.json';

type VideoListProps = {
	count: number,
	items: Array<VideoTrackModel>,
	handleOpenVideo: Function,
	addedVideos: Array<number>,
	handleAddVideo: Function
};

export class VideoList extends Component {

  render() {
    return (
      <List display-if={this.props.count} style={style.container}>
        <Subheader inset>{`Найдено ${this.props.count} видеозаписей`}</Subheader>
        {map(this.props.items, (item: VideoTrackModel, index: number) => (
          <VideoListItem
            key={index}
            data={item}
            isAdded={(indexOf(this.props.addedVideos, item.id)) >= 0}
            handleOpenVideo={this.props.handleOpenVideo}
            handleAddVideo={this.props.handleAddVideo}
          />
				))}
      </List>
    );
  }

  props: VideoListProps;
}

export default VideoList;