// @flow
import React from 'react';
import { GridTile } from 'material-ui/GridList/index';
import type { VideoTrackModel } from '../../types/VideoTrackModel';

import VideoItemMenu from './VideoItemMenu';

import style from './style.json';

type VideoItemProps = {
	item: VideoTrackModel,
	onEdit: Function,
	onDelete: Function,
  onOpenVideo: Function
}

const VideoItem = ({ item, onEdit, onDelete, onOpenVideo }: VideoItemProps) => {
  const src = item.photo_320 || item.photo_130;
  let min;
  let sec;
  if (item.duration) {
    min = Math.ceil(item.duration / 60);
    sec = item.duration % 60 < 10 ? `0${item.duration % 60}` : item.duration % 60;
  } else {
    min = '00';
    sec = '00';
  }

  const subtitle = `${min}:${sec}`;

  return (
    <GridTile
      key={item.id}
      title={item.title}
      subtitle={subtitle}
      actionIcon={<VideoItemMenu onEdit={onEdit} onDelete={onDelete} item={item} />}
      actionPosition="right"
      titlePosition="bottom"
      titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
      cols={1}
      rows={1}
    >
      <img
        key="item-image"
        display-if={src}
        src={src}
        alt={item.description}
        onClick={() => { onOpenVideo(item); }}
        style={style.itemContent}
      />
    </GridTile>
  );
};

export default VideoItem;