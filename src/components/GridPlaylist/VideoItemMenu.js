// @flow
import React from 'react';
import IconMenu from 'material-ui/IconMenu/index';
import MenuItem from 'material-ui/MenuItem/index';
import IconButton from 'material-ui/IconButton/index';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import type { VideoTrackModel } from '../../types/VideoTrackModel';

type VideoItemMenuProps = {
	item: VideoTrackModel,
	onEdit: Function,
	onDelete: Function,
}

const VideoItemMenu = ({ item, onEdit, onDelete }: VideoItemMenuProps) => (
  <IconMenu
    iconButtonElement={<IconButton><MoreVertIcon color="white" /></IconButton>}
    anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
  >
    <MenuItem key="edit-item" onClick={() => { onEdit(item); }} primaryText="Редактировать" />
    <MenuItem key="delete-item" onClick={() => { onDelete(item); }} primaryText="Удалить" />
  </IconMenu>
	);

export default VideoItemMenu;