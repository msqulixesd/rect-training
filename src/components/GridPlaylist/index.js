// @flow
import React, { Component } from 'react';
import { map, cloneDeep } from 'lodash';
import { GridList } from 'material-ui/GridList/index';
import FlatButton from 'material-ui/FlatButton/index';
import Clearfix from 'material-ui/internal/ClearFix';
import Subheader from 'material-ui/Subheader/index';

import type { VideoTrackModel } from '../../types/VideoTrackModel';

import VideoItem from './VideoItem';
import EditVideoItemDialog from '../../components/EditVideoItemDialog';

import style from './style.json';

type GridPlaylistProps = {
	items: Array<VideoTrackModel>,
	count: number,
	updateItem: Function,
	handleOpenVideo: Function,
	deleteVideo: Function,
}

type GridPlaylistState = {
	editedItem: ?VideoTrackModel
};

export class GridPlaylist extends Component {

  static defaultProps = {
    items: []
  };

  state: GridPlaylistState = {
    editedItem: null
  };

  render() {
    return (
      <Clearfix>
        <GridList cols={6} style={style.container}>
          <Subheader inset>{`${this.props.count} видеозаписи(ей)`}</Subheader>
          {map(this.props.items, item => (
            <VideoItem
              key={`video-${item.id}`}
              item={item}
              onEdit={this.handleItemEdit}
              onDelete={this.handleItemDelete}
              onOpenVideo={this.props.handleOpenVideo}
            />)
					)}
          <EditVideoItemDialog
            key="video-dialog"
            actions={[
              <FlatButton key="update-button" primary onClick={this.handleUpdateItem} label="Сохранить" />,
              <FlatButton key="cancel-button" secondary onClick={this.handleCloseEditItemDialog} label="Отмена" />,
            ]}
            item={this.state.editedItem}
            handleClose={this.handleCloseEditItemDialog}
            handleItemChange={this.handleEditedItemChange}
          />
        </GridList>
      </Clearfix>
    );
  }

  handleItemEdit = (item: VideoTrackModel) => {
    this.setState({ editedItem: cloneDeep(item) });
  };

  handleCloseEditItemDialog = () => {
    this.setState({ editedItem: null });
  };

  handleEditedItemChange = (item: VideoTrackModel) => {
    this.setState({ editedItem: item });
  };

  handleUpdateItem = () => {
    const { editedItem } = this.state;
    const { updateItem } = this.props;
    updateItem(editedItem);
    this.handleCloseEditItemDialog();
  };

  handleItemDelete = (item: VideoTrackModel) => {
    const { deleteVideo } = this.props;
    deleteVideo(item);
  };

  props: GridPlaylistProps;
}

export default GridPlaylist;