// @flow
import React, { Component } from 'react';
import Snackbar from 'material-ui/Snackbar/index';

type HintProps = {
	message: ?string
};

type HintState = {
	open: boolean
};

export class Hint extends Component {

  state: HintState = {
    open: false
  };

  componentWillReceiveProps(nextProps: HintProps) {
    if (nextProps.message !== this.props.message) {
      this.setState({ open: true });
    }
  }

  render() {
    return (
      <Snackbar
        open={this.state.open}
        message={this.props.message || ''}
        autoHideDuration={4000}
        onRequestClose={this.handleRequestClose}
      />
    );
  }

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  props: HintProps;
}

export default Hint;