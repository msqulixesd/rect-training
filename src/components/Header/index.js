// @flow
import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar/index';
import Avatar from 'material-ui/Avatar/index';


export type HeaderProps = {
	username?: string,
  avatar?: string,

  initialize?: Function
};

export class Header extends Component {

  static defaultProps = {
    username: '',
    avatar: ''
  };

  componentWillMount() {
    const { initialize } = this.props;
    if (initialize) {
      initialize();
    }
  }

  render() {
    return (
      <AppBar
        display-if={this.props.username}
        title={this.renderTitle()}
        iconElementLeft={this.renderAvatar()}
      />
    );
  }

  renderTitle() {
    const { username } = this.props;
    return username
      ? (<span>{`Видеозаписи ${username}`}</span>)
      : null;
  }

  renderAvatar() {
    const { avatar } = this.props;
    return avatar
      ? (<Avatar src={avatar} size={50} />)
      : null;
  }

  props: HeaderProps;
}

export default Header;
