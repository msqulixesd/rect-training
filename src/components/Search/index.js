// @flow
import React, { Component, Children } from 'react';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar/index';
import TextField from 'material-ui/TextField/index';
import FlatButton from 'material-ui/FlatButton/index';
import SearchIcon from 'material-ui/svg-icons/action/search';

import style from './style.json';

type SearchProps = {
	handleSearch: Function,
	children?: Children
};

export class Search extends Component {


  render() {
    return (
      <Toolbar>
        <ToolbarGroup>
          {this.props.children}
        </ToolbarGroup>
        <ToolbarGroup style={style.container}>
          <TextField
            key="text-field"
            hintText="Поиск видео"
            style={style.input}
            ref={(textField) => { this.textField = textField; }}
          />
          <FlatButton
            key="search-button"
            icon={<SearchIcon />}
            onClick={this.handleSearchClick}
          />
        </ToolbarGroup>
      </Toolbar>
    );
  }

  handleSearchClick = () => {
    const { handleSearch } = this.props;
    handleSearch(this.textField.input.value);
  };

  textField: any;

  props: SearchProps;
}

export default Search;