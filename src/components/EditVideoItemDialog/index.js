// @flow
import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/index';
import TextField from 'material-ui/TextField/index';

import type { VideoTrackModel } from '../../types/VideoTrackModel';

import style from './style.json';

type EditVideoItemDialogProps = {
	item?: ?VideoTrackModel,
	handleClose: Function,
	handleItemChange: Function,
	actions: any
};

export class EditVideoItemDialog extends Component {

  render() {
    const { item, handleClose, actions } = this.props;

    return (
      <Dialog
        key="dialog"
        title="Редактирование видеозаписи"
        actions={actions}
        modal={false}
        open={Boolean(item)}
        onRequestClose={handleClose}
        autoDetectWindowHeight
        autoScrollBodyContent
      >
        <TextField
          key="field-title"
          style={style.editField}
          name="title"
          value={item && item.title}
          hintText="Название"
          onChange={this.handleChange}
        />

        <TextField
          key="field-description"
          style={style.editField}
          name="description"
          value={item && item.description}
          multiLine
          rows={3}
          rowsMax={5}
          hintText="Описание"
          onChange={this.handleChange}
        />
      </Dialog>
    );
  }

  handleChange = (event: Event & { currentTarget: HTMLButtonElement }, value: string) => {
    const { handleItemChange, item } = this.props;
    handleItemChange({
      ...item,
      [event.currentTarget.name]: value
    });
  };

  props: EditVideoItemDialogProps;
}

export default EditVideoItemDialog;