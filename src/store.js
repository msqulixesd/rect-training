import { combineReducers, applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-loading-promise-middleware';
import appReducers from './reducers';

export const createMiddleware = () => compose(
  applyMiddleware(thunkMiddleware),
  applyMiddleware(promiseMiddleware),
  isDevMode && window.devToolsExtension ? window.devToolsExtension() : f => f,
);

export const configureStore = (initialState = {}) => {
  const reducers = combineReducers(appReducers);
  const middleware = createMiddleware();
  return createStore(reducers, initialState, middleware);
};
