import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import App from './containers/AppContainer';
import { parseOauthResponse } from './services/oauthService';
import * as apiService from './services/apiService';
import { REDUCER_AUTH_KEY } from './reducers/authReducer';
import { configureStore } from './store';

import type { ApiAuthorizeResponse } from './types/ApiAuthorizeResponse';

injectTapEventPlugin();

const authParams: ?ApiAuthorizeResponse = window.location.hash
	? parseOauthResponse(window.location.hash)
	: null;

apiService.configure(authParams);

const root = document.getElementById('app');

const store = configureStore({
  [REDUCER_AUTH_KEY]: apiService.getAuthState()
});

ReactDOM.render(
  <Provider store={store}>
    <App isAuthorize={apiService.isAuthorized()} />
  </Provider>,
root);