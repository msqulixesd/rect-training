// @flow

interface Line<PointType> {

	start: PointType;

	end: PointType;

	constructor(PointType, PointType): void;

	getLength(): number;

}

type Point2D = {
	x: number,
	y: number
};

type Point3D = {
	x: number,
	y: number,
	z: number
}


class Line2D implements Line<Point2D> {
  start: Point2D;
  end: Point2D;

  constructor(start: Point2D, end: Point2D) {
    this.start = start;
    this.end = end;
  }

  getLength(): number {
    return Math.sqrt(
				((this.start.x - this.end.x) ** 2)
			+ ((this.start.y - this.end.y) ** 2)
		);
  }
}


class Line3D implements Line<Point3D> {
  start: Point3D;
  end: Point3D;

  constructor(start: Point3D, end:Point3D) {
    this.start = start;
    this.end = end;
  }

  getLength(): number {
    return Math.sqrt(
				((this.start.x - this.end.x) ** 2)
			+ ((this.start.y - this.end.y) ** 2)
			+ ((this.start.z - this.end.z) ** 2)
		);
  }

}

const line2d = new Line2D({ x: 1, y: 1 }, { x: 2, y: 2 });
const line3d = new Line3D({ x: 1, y: 1, z: 2 }, { x: 2, y: 2, z: 3 });

console.log(line2d.getLength());
console.log(line3d.getLength());