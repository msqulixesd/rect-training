// @flow
import { createAction } from 'redux-actions';

export const UPDATE_MESSAGE = 'HINT/UPDATE_MESSAGE';

export const updateHintMessage = createAction(UPDATE_MESSAGE);
