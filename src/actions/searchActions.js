// @flow
import { createAction } from 'redux-actions';
import * as videoService from '../services/videoService';
import type { VideoList } from '../types/VideoTrackModel';

export const UPDATE_VIDEO_LIST = 'SEARCH/UPDATE_VIDEO_LIST';

export const updateVideoList = createAction(UPDATE_VIDEO_LIST);

/**
 * search videos
 * @param query
 * @return {function(Function)}
 */
export function searchVideos(query: string) {
  return async (dispatch: Function) => {
    const data: VideoList = await videoService.search(query);
    return dispatch(updateVideoList(data));
  };
}
