// @flow
import { createAction } from 'redux-actions';
import * as usersService from '../services/usersService';
import type { UserModel } from '../types/UserModel';

export const UPDATE_USER = 'USER/UPDATE_USER';

export const updateUser = createAction(UPDATE_USER);

/**
 * get User information
 * @return {function(Function)}
 */
export function getUserInfo() {
  return async (dispatch: Function) => {
    const data: UserModel = await usersService.get();
    return dispatch(updateUser(data));
  };
}