// @flow
import { createAction } from 'redux-actions';
import * as videoService from '../services/videoService';
import type { VideoList, VideoTrackModel } from '../types/VideoTrackModel';


export const UPDATE_VIDEO = 'VIDEO/UPDATE_VIDEO';
export const UPDATE_VIDEO_LIST = 'VIDEO/UPDATE_VIDEO_LIST';
export const DELETE_VIDEO = 'VIDEO/DELETE_VIDEO';
export const ADD_VIDEO = 'VIDEO/ADD_VIDEO';

export const updateVideoItem = createAction(UPDATE_VIDEO);
export const updateVideoItemList = createAction(UPDATE_VIDEO_LIST);
export const deleteVideoItem = createAction(DELETE_VIDEO);
export const addVideoItem = createAction(ADD_VIDEO);

/**
 * get user album videos
 * @param albumId
 * @return {function(Function)}
 */
export function getVideoItemsList(albumId: ?number = null) {
  return async (dispatch: Function) => {
    const data: VideoList = await videoService.get(albumId);
    return dispatch(updateVideoItemList({
      ...data,
      albumId
    }));
  };
}

/**
 * Add video to album
 * @param item
 * @return {function(Function)}
 */
export function addVideo(item: VideoTrackModel) {
  return async (dispatch: Function) => {
    await videoService.add(item);
    dispatch(addVideoItem(item));
    return item;
  };
}

/**
 * Update video item
 * @param item
 * @return {function(Function)}
 */
export function updateVideo(item: VideoTrackModel) {
  return async (dispatch: Function) => {
    await videoService.edit(item);
    dispatch(updateVideoItem(item));
    return item;
  };
}

/**
 * Remove user video
 * @param item
 * @return {function(Function)}
 */
export function deleteVideo(item: VideoTrackModel) {
  return async (dispatch: Function) => {
    await videoService.remove(item);
    dispatch(deleteVideoItem(item));
    return item;
  };
}