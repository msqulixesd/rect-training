import { get } from 'lodash';
import { handleActions } from 'redux-actions';
import type { ApiAuthorizeResponse } from '../types/ApiAuthorizeResponse';

export type AuthReducerState = {
  isAuthorized: boolean,
  v: string
} & ApiAuthorizeResponse;

export const REDUCER_AUTH_KEY = 'auth';

export const getIsAuthorized = (state: AuthReducerState) => get(state, 'isAuthorized', false);

export default handleActions({}, {});
