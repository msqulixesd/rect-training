import { get } from 'lodash';
import { handleActions } from 'redux-actions';
import { UPDATE_USER } from '../actions/userActions';

import type { UserModel } from '../types/UserModel';

export type UserReducerState = UserModel;

export const initialState = {};

export const REDUCER_USER_KEY = 'user';

export const getUsername = (state: UserReducerState): string => {
  const firstName = get(state, 'first_name', '');
  const lastName = get(state, 'last_name', '');
  return `${firstName} ${lastName}`;
};

export const getUserAvatar = (state: UserReducerState): string => get(state, 'photo_50');

export default handleActions({

  [UPDATE_USER](state: UserReducerState, { payload }: { payload: UserModel }): UserReducerState {
    return payload;
  }

}, initialState);
