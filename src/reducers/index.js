import authReducer, { REDUCER_AUTH_KEY, AuthReducerState } from './authReducer';
import hintReducer, { REDUCER_HINT_KEY, HintReducerState } from './hintReducer';
import videoReducer, { REDUCER_VIDEO_KEY, VideoReducerState } from './videoReducer';
import searchReducer, { REDUCER_SEARCH_KEY, SearchReducerState } from './searchReducer';
import userReducer, { REDUCER_USER_KEY, UserReducerState } from './userReducer';

export type AppState = {
  auth: AuthReducerState,
  hint: HintReducerState,
  video: VideoReducerState,
  search: SearchReducerState,
  user: UserReducerState
}

export default {
  [REDUCER_AUTH_KEY]: authReducer,
  [REDUCER_HINT_KEY]: hintReducer,
  [REDUCER_VIDEO_KEY]: videoReducer,
  [REDUCER_SEARCH_KEY]: searchReducer,
  [REDUCER_USER_KEY]: userReducer
};
