import { get, concat, filter, findIndex, map } from 'lodash';
import { handleActions } from 'redux-actions';
import { VideoList, VideoTrackModel } from '../types/VideoTrackModel';
import {
  UPDATE_VIDEO_LIST,
  ADD_VIDEO,
  UPDATE_VIDEO,
  DELETE_VIDEO
} from '../actions/videoActions';

export type VideoReducerState = {
  albumId?: number
} & VideoList;

export const initialState = {};

export const REDUCER_VIDEO_KEY = 'video';

export const getVideoList = (state: VideoReducerState) => get(state, 'items', []);
export const getVideoCount = (state: VideoReducerState) => get(state, 'count', 0);
export const getAlbumId = (state: VideoReducerState) => get(state, 'albumId', null);
export const getVideosIds = (state: VideoReducerState) => map(getVideoList(state), (item: VideoTrackModel) => item.id);

export default handleActions({

  [UPDATE_VIDEO_LIST](
    state: VideoReducerState,
    { payload }: { payload: VideoReducerState }): VideoReducerState {
    return payload;
  },

  [ADD_VIDEO](
    state: VideoReducerState,
    { payload }: { payload: VideoTrackModel }): VideoReducerState {
    return {
      ...state,
      items: concat([payload], getVideoList(state)),
      count: getVideoCount(state) + 1,
    };
  },

  [UPDATE_VIDEO](
    state: VideoReducerState,
    { payload }: { payload: VideoTrackModel }): VideoReducerState {
    const items = getVideoList(state);
    const itemIndex = findIndex(items, { id: payload.id });

    if (itemIndex !== undefined) {
      items[itemIndex] = payload;
    }

    return {
      ...state,
      items: concat([], items)
    };
  },

  [DELETE_VIDEO](
    state: VideoReducerState,
    { payload }: { payload: VideoTrackModel }): VideoReducerState {
    return {
      ...state,
      items: filter(getVideoList(state), item => item.id !== payload.id),
      count: getVideoCount(state) - 1
    };
  }
}, initialState);
