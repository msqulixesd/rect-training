import { get } from 'lodash';
import { handleActions } from 'redux-actions';
import { VideoList } from '../types/VideoTrackModel';
import {
  UPDATE_VIDEO_LIST,
} from '../actions/searchActions';

export type SearchReducerState = VideoList;

export const initialState: SearchReducerState = {
  count: 0,
  items: []
};

export const REDUCER_SEARCH_KEY = 'search';

export const getVideoList = (state: SearchReducerState) => get(state, 'items', []);
export const getVideoCount = (state: SearchReducerState) => get(state, 'count', 0);

export default handleActions({

  [UPDATE_VIDEO_LIST](
    state: SearchReducerState,
    { payload }: { payload: VideoList }): SearchReducerState {
    return payload;
  }

}, initialState);
