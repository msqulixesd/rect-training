import { get } from 'lodash';
import { handleActions } from 'redux-actions';
import { UPDATE_MESSAGE } from '../actions/hintActions';

export type HintReducerState = {
  message: ?string
};

export const initialState = {
  message: null
};

export const REDUCER_HINT_KEY = 'hint';

export const getHintMessage = (state: HintReducerState) => get(state, 'message', null);

export default handleActions({
  [UPDATE_MESSAGE](state: HintReducerState, { payload }: { payload: string }): HintReducerState {
    return {
      ...state,
      message: payload
    };
  }
}, initialState);
