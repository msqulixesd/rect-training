// @flow

export type ApiRequest = {
	access_token?: string,
	v?: string,
};