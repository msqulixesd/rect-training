// @flow

import {
	AUTH_DISPLAY_TYPE_PAGE,
	AUTH_DISPLAY_TYPE_POPUP,
	AUTH_DISPLAY_TYPE_MOBILE,
	APP_SCOPE_AUDIO,
	AUTH_TYPE
} from '../config';


export type AuthDisplayType = AUTH_DISPLAY_TYPE_PAGE | AUTH_DISPLAY_TYPE_POPUP | AUTH_DISPLAY_TYPE_MOBILE;
export type AuthScopeType = APP_SCOPE_AUDIO;
export type AuthResponseType = AUTH_TYPE;

export type ApiAuthorizeRequest = {
	client_id: number,
	display: AuthDisplayType,
	redirect_uri: string,
	scope: AuthScopeType,
	response_type: AuthResponseType,
	v: string,
	revoke: 1 | 0
};