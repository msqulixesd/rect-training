// @flow

export type VideoTrackModel = {
	id: number,
	owner_id: number,
	title: string,
	description: string,
	duration: number,
	photo_130?: string,
	photo_320?: string,
	photo_640?: string,
	photo_800?: string,
	date: number,
	adding_date?: number,
	views: number,
	comments: number,
	player: string,
	access_key?: string,
	processing?: 1,
	live?: 1
};

export type VideoList = {
	count: number,
	items: Array<VideoTrackModel>
}