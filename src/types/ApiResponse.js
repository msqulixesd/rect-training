// @flow

export type ApiResponse<Model> = {
	response: Model
}