// @flow

export type ApiAuthorizeResponse = {
	access_token: string,
	expires_in: number,
	user_id: number
};