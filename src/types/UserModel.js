// @flow
export type UserModel = {
	id: number,
	first_name: string,
	last_name: string,
	photo_50: string,
};