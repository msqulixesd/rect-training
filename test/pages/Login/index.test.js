import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Login } from '../../../src/pages/Login/index';

const SNAPSHOT = 'SNAPSHOT';

const loginUrl = 'http://loginUrl';

describe('pages/Login/index.js', () => {
  it('snapshot', () => {
    const wrapper = shallow(<Login authUrl={loginUrl} />);
    expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
  });
});
