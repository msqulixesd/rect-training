import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { App } from '../../../src/pages/App/index';

const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_AUTHORIZED = 'SNAPSHOT_AUTHORIZED';

describe('pages/App/index.js', () => {
  describe('snapshots', () => {
    it('snapshot', () => {
      const wrapper = shallow(<App />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });

    it('authorized', () => {
      const wrapper = shallow(<App isAuthorize />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_AUTHORIZED);
    });
  });
});
