import { first } from 'lodash';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { getMockData } from '../../../testing/helpers/mock';
import { findByKey } from '../../../testing/helpers/helpers';
import type { VideoTrackModel, VideoList } from '../../../src/types/VideoTrackModel';
import type { ApiResponse } from '../../../src/types/ApiResponse';

import type { PlayerProps } from '../../../src/pages/Player/index';
import { Player } from '../../../src/pages/Player/index';

const MENU_BUTTON_KEY = 'menu';

const data: ApiResponse<VideoList> = getMockData('video.get');
const items: Array<VideoTrackModel> = data.response.items;
const count = data.response.count;
const firstItem: VideoTrackModel = first(items);

const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_OPENED = 'SNAPSHOT_OPENED';
const SNAPSHOT_VIDEO = 'SNAPSHOT_VIDEO';

const props: PlayerProps = {
  hintMessage: 'hint message',
  videoCount: count,
  userVideos: items,
  addedVideos: [firstItem.id],
  searchCount: count,
  searchResult: items,

  initialize: jest.fn(),
  addVideo: jest.fn(),
  editVideo: jest.fn(),
  deleteVideo: jest.fn(),
  searchVideos: jest.fn(),
};

describe('pages/Player/index.js', () => {
  describe('snapshots', () => {
    it('with props', () => {
      const wrapper = shallow(<Player {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });

    it('opened', () => {
      const wrapper = shallow(<Player {...props} />);
      wrapper.setState({ isVideoContainerOpened: true });
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_OPENED);
    });

    it('with video', () => {
      const wrapper = shallow(<Player {...props} />);
      wrapper.setState({ playingVideo: firstItem });
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_VIDEO);
    });
  });

  describe('lifecyrcle methods', () => {
    it('componentWillMount', () => {
      const initialize = jest.fn();
      shallow(<Player {...props} initialize={initialize} />);
      expect(initialize).toBeCalled();
    });
  });

  describe('methods', () => {
    let wrapper;

    beforeEach(() => { wrapper = shallow(<Player {...props} />); });

    it('handleClosePlayingVideo', () => {
      wrapper.setState({ playingVideo: firstItem });
      expect(wrapper.state('playingVideo')).toEqual(firstItem);
      wrapper.instance().handleClosePlayingVideo();
      expect(wrapper.state('playingVideo')).toEqual(null);
    });

    it('handleOpenVideo', () => {
      expect(wrapper.state('playingVideo')).toEqual(null);
      wrapper.instance().handleOpenVideo(firstItem);
      expect(wrapper.state('playingVideo')).toEqual(firstItem);
    });
  });

  describe('actions', () => {
    it('onClick MenuIcon', () => {
      const wrapper = shallow(<Player {...props} />);
      let menuButton = findByKey(wrapper, MENU_BUTTON_KEY).first();
      expect(wrapper.state('isVideoContainerOpened')).toEqual(true);
      menuButton.simulate('click');
      expect(wrapper.state('isVideoContainerOpened')).toEqual(false);
      /* TODO: А вот тут ошибка в реализации!!! */
      menuButton = findByKey(wrapper, MENU_BUTTON_KEY).first();
      menuButton.simulate('click');
      expect(wrapper.state('isVideoContainerOpened')).toEqual(true);
    });
  });
});
