import searchReducer, { initialState } from '../../src/reducers/searchReducer';
import { UPDATE_VIDEO_LIST, updateVideoList } from '../../src/actions/searchActions';
import { MockStore } from '../../testing/helpers/mock';

const state = new MockStore();

describe('reducers/hintReducer.js', () => {
  it('initialState', () => {
    expect(searchReducer(undefined, {})).toEqual(initialState);
  });

  it(UPDATE_VIDEO_LIST, () => {
    expect(searchReducer(initialState, updateVideoList(state.search))).toEqual(state.search);
  });
});