import hintReducer, { initialState } from '../../src/reducers/hintReducer';
import { UPDATE_MESSAGE, updateHintMessage } from '../../src/actions/hintActions';

describe('/reducers/hintReducer.js', () => {
  it('initialState', () => {
    expect(hintReducer(undefined, {})).toEqual(initialState);
  });

  it(UPDATE_MESSAGE, () => {
    const message = 'Test message';
    expect(hintReducer(initialState, updateHintMessage(message))).toEqual({ ...initialState, message });
  });
});