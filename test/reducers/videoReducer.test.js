import videoReducer, { initialState } from '../../src/reducers/videoReducer';
import {
  UPDATE_VIDEO_LIST,
  ADD_VIDEO,
  UPDATE_VIDEO,
  DELETE_VIDEO,
  updateVideoItemList,
  addVideoItem,
  updateVideoItem,
  deleteVideoItem
} from '../../src/actions/videoActions';
import { MockStore } from '../../testing/helpers/mock';

const state = new MockStore();

describe('reducers/videoReducer.js', () => {
  it('initialState', () => {
    expect(videoReducer(undefined, {})).toEqual(initialState);
  });

  it(UPDATE_VIDEO_LIST, () => {
    expect(videoReducer(initialState, updateVideoItemList(state.video))).toEqual(state.video);
  });

  it(ADD_VIDEO, () => {
    const item = state.getFirstVideoItem();
    const expected = {
      ...initialState,
      count: 1,
      items: [item]
    };
    expect(videoReducer(initialState, addVideoItem(item))).toEqual(expected);
  });

  it(UPDATE_VIDEO, () => {
    const title = 'Test item title';
    const newList = state.cloneVideoList();
    newList[0].title = title;
    const expected = {
      items: newList,
      count: state.video.count
    };
    expect(videoReducer(state.video, updateVideoItem(newList[0]))).toEqual(expected);
  });

  it(DELETE_VIDEO, () => {
    const newList = state.cloneVideoList();
    const item = newList[0];
    newList.shift();
    const expected = {
      items: newList,
      count: newList.length
    };
    expect(videoReducer(state.video, deleteVideoItem(item))).toEqual(expected);
  });
});