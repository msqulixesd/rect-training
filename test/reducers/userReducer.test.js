import userReducer, { initialState } from '../../src/reducers/userReducer';
import { UPDATE_USER, updateUser } from '../../src/actions/userActions';
import { MockStore } from '../../testing/helpers/mock';

const state = new MockStore();

describe('reducers/userReducer.js', () => {
  it('initialState', () => {
    expect(userReducer(undefined, {})).toEqual(initialState);
  });

  it(UPDATE_USER, () => {
    expect(userReducer(initialState, updateUser(state.user))).toEqual(state.user);
  });
});