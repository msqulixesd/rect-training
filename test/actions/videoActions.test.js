import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getMockData } from '../../testing/helpers/mock';
import * as videoService from '../../src/services/videoService';
import {
  getVideoItemsList,
  addVideo,
  updateVideo,
  deleteVideo,

  updateVideoItemList,
  updateVideoItem,
  deleteVideoItem,
  addVideoItem
   } from '../../src/actions/videoActions';

const videoListData = getMockData('video.get');
const videoItemData = getMockData('videoTrack');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions/videoActions.js', () => {
  const videoGetSpy = jest.spyOn(videoService, 'get');
  const videoAddSpy = jest.spyOn(videoService, 'add');
  const videoEditSpy = jest.spyOn(videoService, 'edit');
  const videoRemoveSpy = jest.spyOn(videoService, 'remove');

  it('getVideoItemsList action', async () => {
    const store = mockStore({});
    const albumId = 123;
    const expectedActions = [updateVideoItemList({
      ...videoListData.response,
      albumId
    })];

    await store.dispatch(getVideoItemsList(albumId));
    expect(store.getActions()).toEqual(expectedActions);
    expect(videoGetSpy).toBeCalledWith(albumId);
  });

  it('addVideo action', async () => {
    const store = mockStore({});
    const expectedActions = [addVideoItem(videoItemData)];

    await store.dispatch(addVideo(videoItemData));
    expect(store.getActions()).toEqual(expectedActions);
    expect(videoAddSpy).toBeCalledWith(videoItemData);
  });

  it('updateVideo action', async () => {
    const store = mockStore({});
    const expectedActions = [updateVideoItem(videoItemData)];

    await store.dispatch(updateVideo(videoItemData));
    expect(store.getActions()).toEqual(expectedActions);
    expect(videoEditSpy).toBeCalledWith(videoItemData);
  });

  it('deleteVideo action', async () => {
    const store = mockStore({});
    const expectedActions = [deleteVideoItem(videoItemData)];

    await store.dispatch(deleteVideo(videoItemData));
    expect(store.getActions()).toEqual(expectedActions);
    expect(videoRemoveSpy).toBeCalledWith(videoItemData);
  });
});
