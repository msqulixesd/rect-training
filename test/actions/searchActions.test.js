import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getMockData } from '../../testing/helpers/mock';
import * as videoService from '../../src/services/videoService';
import { searchVideos, updateVideoList } from '../../src/actions/searchActions';

const searchResultData = getMockData('video.search');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions/searchActions.js', () => {
  const searchSpy = jest.spyOn(videoService, 'search');

  it('searchVideos action', async () => {
    const expectedActions = [updateVideoList(searchResultData.response)];
    const store = mockStore({});
    const query = 'test query';
    await store.dispatch(searchVideos(query));
    expect(store.getActions()).toEqual(expectedActions);
    expect(searchSpy).toBeCalledWith(query);
  });
});
