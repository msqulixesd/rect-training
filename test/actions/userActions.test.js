import { first } from 'lodash';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getMockData } from '../../testing/helpers/mock';
import * as usersService from '../../src/services/usersService';
import { getUserInfo, updateUser } from '../../src/actions/userActions';

const usersData = getMockData('users.get');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions/userActions.js', () => {
  const usersGetSpy = jest.spyOn(usersService, 'get');

  it('getUserInfo action', async () => {
    const expectedActions = [updateUser(first(usersData.response))];
    const store = mockStore({});
    await store.dispatch(getUserInfo());
    expect(store.getActions()).toEqual(expectedActions);
    expect(usersGetSpy).toBeCalled();
  });
});
