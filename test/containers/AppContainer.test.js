import React from 'react';
import toJson from 'enzyme-to-json';
import { MockStore } from '../../testing/helpers/mock';
import { getContainerWrapper } from '../../testing/helpers/helpers';
import AppContainer, { mapStateToProps } from '../../src/containers/AppContainer';

const state = new MockStore();

describe('containers/AppContainer.js', () => {
  it('mapStateToProps', () => {
    expect(mapStateToProps(state)).toEqual({
      isAuthorize: state.auth.isAuthorized
    });
  });

  it('snapshot', () => {
    const wrapper = getContainerWrapper(<AppContainer />, state);
    expect(toJson(wrapper)).toMatchSnapshot('AppContainer');
  });
});