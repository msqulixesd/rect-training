import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import toJson from 'enzyme-to-json';
import { MockStore, getMockData } from '../../testing/helpers/mock';
import { getContainerWrapper } from '../../testing/helpers/helpers';
import PlayerContainer, { mapStateToProps, mapDispatchToProps } from '../../src/containers/PlayerContainer';
import {
  updateVideoItemList,
  addVideoItem,
  updateVideoItem,
  deleteVideoItem
} from '../../src/actions/videoActions';
import { updateVideoList } from '../../src/actions/searchActions';
import { updateHintMessage } from '../../src/actions/hintActions';

const state = new MockStore();

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const videoListData = getMockData('video.get').response;
const videoTrackData = getMockData('videoTrack');
const videoSearchData = getMockData('video.search').response;

describe('containers/PlayerContainer.js', () => {
  it('mapStateToProps', () => {
    expect(mapStateToProps(state)).toEqual({
      hintMessage: state.hint.message,
      videoCount: state.video.count,
      userVideos: state.video.items,
      addedVideos: state.getVideosIds(),
      searchCount: state.search.count,
      searchResult: state.search.items
    });
  });

  describe('mapDispatchToProps', () => {
    let store;
    let actions;

    beforeEach(() => {
      store = mockStore({});
      actions = mapDispatchToProps(store.dispatch);
    });

    it('initialize', async () => {
      const expected = [updateVideoItemList({ ...videoListData, albumId: null })];
      await actions.initialize();
      expect(store.getActions()).toEqual(expected);
    });

    it('addVideo', async () => {
      const expected = [
        addVideoItem(videoTrackData),
        updateHintMessage(`Видеозапись "${videoTrackData.title}" добавлена`)
      ];
      await actions.addVideo(videoTrackData);
      expect(store.getActions()).toEqual(expected);
    });

    it('editVideo', async () => {
      const expected = [
        updateVideoItem(videoTrackData),
        updateHintMessage(`Видеозапись "${videoTrackData.title}" обновлена`)
      ];
      await actions.editVideo(videoTrackData);
      expect(store.getActions()).toEqual(expected);
    });

    it('deleteVideo', async () => {
      const expected = [
        deleteVideoItem(videoTrackData),
        updateHintMessage(`Видеозапись "${videoTrackData.title}" удалена`)
      ];
      await actions.deleteVideo(videoTrackData);
      expect(store.getActions()).toEqual(expected);
    });

    it('searchVideos', async () => {
      const expected = [updateVideoList(videoSearchData)];
      await actions.searchVideos('query');
      expect(store.getActions()).toEqual(expected);
    });
  });

  it('snapshot', () => {
    const wrapper = getContainerWrapper(<PlayerContainer />, state);
    expect(toJson(wrapper)).toMatchSnapshot('PlayerContainer');
  });
});