import { first } from 'lodash';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import toJson from 'enzyme-to-json';
import { MockStore, getMockData } from '../../../testing/helpers/mock';
import { getContainerWrapper } from '../../../testing/helpers/helpers';
import HeaderContainer, {
  mapStateToProps,
  mapDispatchToProps
} from '../../../src/containers/components/HeaderContainer';
import { updateUser } from '../../../src/actions/userActions';

const state = new MockStore();

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const userData = first(getMockData('users.get').response);

describe('containers/components/HeaderContainer.js', () => {
  it('mapStateToProps', () => {
    expect(mapStateToProps(state)).toEqual({
      username: state.getUserName(),
      avatar: state.getUserAvatar()
    });
  });

  describe('mapDispatchToProps', () => {
    let store;
    let actions;

    beforeEach(() => {
      store = mockStore({});
      actions = mapDispatchToProps(store.dispatch);
    });

    it('initialize', async () => {
      const expected = [updateUser(userData)];
      await actions.initialize();
      expect(store.getActions()).toEqual(expected);
    });
  });

  it('snapshot', () => {
    const wrapper = getContainerWrapper(<HeaderContainer />, state);
    expect(toJson(wrapper)).toMatchSnapshot('HeaderContainer');
  });
});