import { first } from 'lodash';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { getMockData } from '../../../testing/helpers/mock';
import { findByKey } from '../../../testing/helpers/helpers';
import type { VideoTrackModel } from '../../../src/types/VideoTrackModel';

import { GridPlaylist } from '../../../src/components/GridPlaylist/index';

const geItemKey = (item: VideoTrackModel) => `video-${item.id}`;

const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_OPENED_DIALOG = 'SNAPSHOT_OPENED_DIALOG';

const data = getMockData('video.get');

const count = data.response.count;
const items = data.response.items;

describe('components/GridPlaylist/index.js', () => {
  describe('snapshots', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(
        <GridPlaylist
          items={items}
          count={count}
          updateItem={jest.fn()}
          handleOpenVideo={jest.fn()}
          deleteVideo={jest.fn()}
        />);
    });

    it('snapshot', () => {
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });

    it('snapshot', () => {
      const editedItem = first(items);
      wrapper.setState({ editedItem });
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_OPENED_DIALOG);
    });
  });


  describe('methods', () => {
    let wrapper;
    const item = first(items);
    const updateItem = jest.fn();
    const handleOpenVideo = jest.fn();
    const deleteVideo = jest.fn();

    beforeEach(() => {
      updateItem.mockClear();
      handleOpenVideo.mockClear();
      deleteVideo.mockClear();
      wrapper = shallow(
        <GridPlaylist
          items={items}
          count={count}
          updateItem={updateItem}
          handleOpenVideo={handleOpenVideo}
          deleteVideo={deleteVideo}
        />);
    });

    it('handleItemEdit', () => {
      expect(wrapper.state('editedItem')).toEqual(null);
      wrapper.instance().handleItemEdit(item);
      expect(wrapper.state('editedItem')).toEqual(item);
    });

    it('handleCloseEditItemDialog', () => {
      wrapper.setState({ editedItem: item });
      expect(wrapper.state('editedItem')).toEqual(item);
      wrapper.instance().handleCloseEditItemDialog();
      expect(wrapper.state('editedItem')).toEqual(null);
    });

    it('handleEditedItemChange', () => {
      expect(wrapper.state('editedItem')).toEqual(null);
      wrapper.instance().handleEditedItemChange(item);
      expect(wrapper.state('editedItem')).toEqual(item);
    });

    it('handleUpdateItem', () => {
      const instance = wrapper.instance();
      wrapper.setState({ editedItem: item });
      expect(wrapper.state('editedItem')).toEqual(item);
      instance.handleCloseEditItemDialog = jest.fn();
      instance.handleUpdateItem();
      expect(instance.handleCloseEditItemDialog).toBeCalled();
      expect(updateItem).toBeCalledWith(item);
    });

    it('handleItemDelete', () => {
      wrapper.instance().handleItemDelete(item);
      expect(deleteVideo).toBeCalledWith(item);
    });
  });

  describe('actions', () => {
    const item = first(items);
    let wrapper;
    const itemKey = geItemKey(item);
    let itemElement;
    const handleOpenVideo = jest.fn();
    const updateItem = jest.fn();
    const deleteVideo = jest.fn();

    beforeEach(() => {
      handleOpenVideo.mockClear();
      updateItem.mockClear();
      deleteVideo.mockClear();
      wrapper = shallow(
        <GridPlaylist
          items={items}
          count={count}
          updateItem={handleOpenVideo}
          handleOpenVideo={handleOpenVideo}
          deleteVideo={deleteVideo}
        />);
      itemElement = findByKey(wrapper, itemKey).first();
    });

    it('onOpenVideo', () => {
      itemElement.simulate('OpenVideo', item);
      expect(handleOpenVideo).toBeCalledWith(item);
    });

    it('onEdit', () => {
      itemElement.simulate('Edit', item);
      expect(wrapper.state('editedItem')).toEqual(item);
    });

    it('onDelete', () => {
      itemElement.simulate('Delete', item);
      expect(deleteVideo).toBeCalledWith(item);
    });
  });
});
