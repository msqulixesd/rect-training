import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { getMockData } from '../../../testing/helpers/mock';
import { findByKey } from '../../../testing/helpers/helpers';
import type { VideoTrackModel } from '../../../src/types/VideoTrackModel';

import VideoItemMenu from '../../../src/components/GridPlaylist/VideoItemMenu';

const EDIT_ITEM_KEY = 'edit-item';
const DELETE_ITEM_KEY = 'delete-item';

const item: VideoTrackModel = getMockData('videoTrack');

const SNAPSHOT = 'SNAPSHOT';

describe('components/GridPlaylist/VideoItem.js', () => {
  describe('snapshots', () => {
    it('snapshot', () => {
      const wrapper = shallow(<VideoItemMenu
        item={item}
        onEdit={jest.fn()}
        onDelete={jest.fn()}
      />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });
  });

  describe('actions', () => {
    const onEdit = jest.fn();
    const onDelete = jest.fn();
    let wrapper;

    beforeEach(() => {
      onEdit.mockClear();
      onDelete.mockClear();
      wrapper = shallow(<VideoItemMenu item={item} onEdit={onEdit} onDelete={onDelete} />);
    });

    it('onEditItem', () => {
      const editButton = findByKey(wrapper, EDIT_ITEM_KEY).first();
      editButton.simulate('click');
      expect(onEdit).toBeCalledWith(item);
    });

    it('onDelete', () => {
      const deleteButton = findByKey(wrapper, DELETE_ITEM_KEY).first();
      deleteButton.simulate('click');
      expect(onDelete).toBeCalledWith(item);
    });
  });
});