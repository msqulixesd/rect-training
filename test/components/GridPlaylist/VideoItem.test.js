import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { findByKey } from '../../../testing/helpers/helpers';
import type { VideoTrackModel } from '../../../src/types/VideoTrackModel';

import VideoItem from '../../../src/components/GridPlaylist/VideoItem';

const ITEM_IMAGE_KEY = 'item-image';

const itemProps: VideoTrackModel = {
  id: 163496448,
  owner_id: 11673524,
  title: 'Track title',
  duration: 0,
  description: '1231231',
  date: 1348002473,
  comments: 0,
  views: 10,
  adding_date: 1348002473,
  player: 'https://playerUri',
  can_edit: 1,
  can_add: 1
};

const photo320 = 'https://photo_320';
const photo130 = 'https://photo_130';
const duration = 613;

const itemFullProps = {
  ...itemProps,
  photo_130: photo130,
  photo_320: photo320,
  duration
};

const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_PHOTO_130 = 'SNAPSHOT_PHOTO_130';
const SNAPSHOT_PHOTO = 'SNAPSHOT_PHOTO';
const SNAPSHOT_DURATION = 'SNAPSHOT_DURATION';
const SNAPSHOT_DURATION_SEC = 'SNAPSHOT_DURATION_SEC';

describe('components/GridPlaylist/VideoItem.js', () => {
  describe('snapshots', () => {
    let defaultProps;
    beforeEach(() => {
      defaultProps = {
        item: itemProps,
        onEdit: jest.fn(),
        onDelete: jest.fn(),
        onOpenVideo: jest.fn()
      };
    });

    it('default snapshot', () => {
      const wrapper = shallow(<VideoItem {...defaultProps} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });

    it('snapshot with photo 320', () => {
      const props = {
        ...defaultProps,
        item: {
          ...itemProps,
          photo_130: photo130
        }
      };
      const wrapper = shallow(<VideoItem {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_PHOTO_130);
    });

    it('snapshot with photo', () => {
      const props = {
        ...defaultProps,
        item: {
          ...itemProps,
          photo_320: photo320,
          photo_130: photo130
        }
      };
      const wrapper = shallow(<VideoItem {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_PHOTO);
    });

    it('snapshot with duration', () => {
      const props = {
        ...defaultProps,
        item: {
          ...itemProps,
          photo_320: photo320,
          photo_130: photo130,
          duration
        }
      };
      const wrapper = shallow(<VideoItem {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_DURATION);
    });

    it('snapshot with duration with secs less than 10', () => {
      const props = {
        ...defaultProps,
        item: {
          ...itemProps,
          photo_320: photo320,
          photo_130: photo130,
          duration: 65
        }
      };
      const wrapper = shallow(<VideoItem {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_DURATION_SEC);
    });
  });

  describe('actions', () => {
    it('onClick item image', () => {
      const onOpenVideo = jest.fn();
      const wrapper = shallow(<VideoItem
        item={itemFullProps}
        onEdit={jest.fn()}
        onDelete={jest.fn()}
        onOpenVideo={onOpenVideo}
      />);
      const image = findByKey(wrapper, ITEM_IMAGE_KEY).first();
      image.simulate('click');
      expect(onOpenVideo).toBeCalledWith(itemFullProps);
    });
  });
});