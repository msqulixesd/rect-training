import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { getMockData } from '../../../testing/helpers/mock';
import { findByKey } from '../../../testing/helpers/helpers';
import { EditVideoItemDialog } from '../../../src/components/EditVideoItemDialog/index';
import type { VideoTrackModel } from '../../../src/types/VideoTrackModel';

const videoTrack: VideoTrackModel = getMockData('videoTrack');

const EMPTY = 'EMPTY';
const WITH_PROPS = 'WITH_PROPS';

const FIELD_TITLE_KEY = 'field-title';
const FIELD_DESCRIPTION_KEY = 'field-description';
const DIALOG_KEY = 'dialog';

const emptyProps = {
  handleClose: jest.fn(),
  handleItemChange: jest.fn(),
  actions: []
};

const props = {
  handleClose: jest.fn(),
  handleItemChange: jest.fn(),
  actions: [
    <div>Fake action</div>
  ],
  item: videoTrack
};

describe('components/EditVideoItemDialog/index.js', () => {
  describe('snapshots', () => {
    it('empty snapshot', () => {
      const wrapper = shallow(<EditVideoItemDialog {...emptyProps} />);
      expect(toJson(wrapper)).toMatchSnapshot(EMPTY);
    });

    it('with data', () => {
      const wrapper = shallow(<EditVideoItemDialog {...props} />);
      expect(toJson(wrapper)).toMatchSnapshot(WITH_PROPS);
    });
  });

  describe('actions', () => {
    describe('onChange value', () => {
      let wrapper;
      const handleItemChange = jest.fn();

      beforeEach(() => {
        handleItemChange.mockClear();
        wrapper = shallow(<EditVideoItemDialog
          handleClose={jest.fn}
          handleItemChange={handleItemChange}
          actions={[]}
          item={videoTrack}
        />);
      });

      it('change title', () => {
        const name = 'title';
        const value = 'fake title';
        const event = {
          currentTarget: { name }
        };
        const field = findByKey(wrapper, FIELD_TITLE_KEY).first();
        field.simulate('change', event, value);
        expect(handleItemChange).toBeCalledWith({
          ...videoTrack,
          [name]: value
        });
      });

      it('change description', () => {
        const name = 'description';
        const value = 'fake description';
        const event = {
          currentTarget: { name }
        };
        const field = findByKey(wrapper, FIELD_DESCRIPTION_KEY).first();
        field.simulate('change', event, value);
        expect(handleItemChange).toBeCalledWith({
          ...videoTrack,
          [name]: value
        });
      });
    });

    it('onRequestClose', () => {
      const handleClose = jest.fn();
      const wrapper = shallow(<EditVideoItemDialog
        handleClose={handleClose}
        handleItemChange={jest.fn()}
        actions={[]}
      />);
      const dialog = findByKey(wrapper, DIALOG_KEY).first();

      dialog.simulate('RequestClose');
      expect(handleClose).toBeCalled();
    });
  });
});
