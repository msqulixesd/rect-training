import React from 'react';
import { shallow } from 'enzyme';
import { findByKey } from '../../../testing/helpers/helpers';

import { Search } from '../../../src/components/Search/index';

const SEARCH_BUTTON_FIELD = 'search-button';

const SNAPSHOT = 'SNAPSHOT';

describe('components/Search/index.js', () => {
  it('snapshot', () => {
    const wrapper = shallow(
      <Search handleSearch={jest.fn()}>
        <div>{'children content'}</div>
      </Search>
    );

    expect(wrapper).toMatchSnapshot(SNAPSHOT);
  });

  describe('actions', () => {
    it('search button onClick', () => {
      const value = 'search value';
      const handleSearch = jest.fn();
      const wrapper = shallow(<Search handleSearch={handleSearch} />);
      const button = findByKey(wrapper, SEARCH_BUTTON_FIELD).first();
      wrapper.instance().textField = {
        input: { value }
      };
      button.simulate('click');
      expect(handleSearch).toBeCalledWith(value);
    });
  });
});
