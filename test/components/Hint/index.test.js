import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Hint } from '../../../src/components/Hint/index';

const getWrapper = (message?: string, open?: boolean) => {
  const wrapper = shallow(<Hint message={message} />);
  if (open) {
    wrapper.setState({ open });
  }
  return wrapper;
};

const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_EMPTY = 'SNAPSHOT_EMPTY';
const SNAPSHOT_OPENED = 'SNAPSHOT_OPENED';

describe('components/Hint/index.js', () => {
  describe('snapshots', () => {
    it('empty', () => {
      const wrapper = getWrapper();
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_EMPTY);
    });

    it('with message', () => {
      const wrapper = getWrapper('message');
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });

    it('opened', () => {
      const wrapper = getWrapper('message', true);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_OPENED);
    });
  });

  describe('lifecircle methods', () => {
    it('componentWillReceiveProps', () => {
      const message1 = 'message1';
      const message2 = 'message2';

      const wrapper = getWrapper(message1);
      expect(wrapper.state('open')).toEqual(false);

      wrapper.setProps({ message: message1 });
      expect(wrapper.state('open')).toEqual(false);

      wrapper.setProps({ message: message2 });
      expect(wrapper.state('open')).toEqual(true);
    });
  });

  describe('methods', () => {
    it('handleRequestClose', () => {
      const wrapper = getWrapper('message');
      wrapper.setState({ open: true });
      wrapper.instance().handleRequestClose();
      expect(wrapper.state('open')).toEqual(false);
    });
  });
});