import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Header from '../../../src/components/Header/index';
import type { HeaderProps } from '../../../src/components/Header/index';

const SNAPSHOT_EMPTY = 'SNAPSHOT_EMPTY';
const SNAPSHOT = 'SNAPSHOT';
const SNAPSHOT_TITLE = 'SNAPSHOT_TITLE';
const SNAPSHOT_AVATAR = 'SNAPSHOT_AVATAR';

const props: HeaderProps = {
  username: 'User Name',
  avatar: 'http://avatarImage'
};

const getWrapperWithProps = (username?: string, avatar?: string): HeaderProps => {
  const wrapperProps = { username, avatar };
  return shallow(<Header {...wrapperProps} />);
};

describe('components/Header/index.js', () => {
  describe('snapshots', () => {
    it('empty', () => {
      const wrapper = shallow(<Header />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT_EMPTY);
    });

    it('snapshot', () => {
      const wrapper = shallow(<Header {...props} initialize={jest.fn()} />);
      expect(toJson(wrapper)).toMatchSnapshot(SNAPSHOT);
    });
  });

  describe('lifecircle methods', () => {
    it('componentWillMount', () => {
      const initialize = jest.fn();
      shallow(<Header initialize={initialize} />);
      expect(initialize).toBeCalled();
    });
  });

  describe('methods', () => {
    describe('renderTitle', () => {
      it('without title', () => {
        const wrapper = getWrapperWithProps();
        expect(wrapper.instance().renderTitle()).toEqual(null);
      });

      it('with title', () => {
        const wrapper = getWrapperWithProps('username');
        expect(wrapper.instance().renderTitle()).toMatchSnapshot(SNAPSHOT_TITLE);
      });
    });

    describe('renderAvatar', () => {
      it('without avatar', () => {
        const wrapper = getWrapperWithProps();
        expect(wrapper.instance().renderAvatar()).toEqual(null);
      });

      it('with avatar', () => {
        const wrapper = getWrapperWithProps(null, 'avatar URL');
        expect(wrapper.instance().renderAvatar()).toMatchSnapshot(SNAPSHOT_AVATAR);
      });
    });
  });
});
