const webpack = require('webpack');
const path = require('path');

exports.devtool = 'eval';

exports.plugins = [
	new webpack.HotModuleReplacementPlugin()
];

exports.devServer = {
	proxy: {
		'/api/**': {
			target: 'https://api.vk.com',
			pathRewrite: {'^/api' : ''},
			changeOrigin: true
		}
	},
	contentBase: path.join(__dirname, '../dist'),
	historyApiFallback: {
		rewrites: [
			{ from: /./, to: '/index.html' },
		]
	},
	port: 3000,
	compress: false,
	inline: true,
	hot: true,
	https: true,
	stats: {
		assets: true,
		children: false,
		chunks: false,
		hash: false,
		modules: false,
		publicPath: false,
		timings: true,
		version: false,
		warnings: true,
		colors: {
			green: '\u001b[32m',
		}
	},
	headers: {
		"Access-Control-Allow-Origin": "*",
		"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
		"Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
	}
};