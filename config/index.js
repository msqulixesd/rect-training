const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const devConfig = require('./dev.config');
const prodConfig = require('./prod.config');

const ENV_PROD = 'PROD';
const ENV_DEV = 'DEV';

const rootPath = path.join(__dirname, '../');
const sourcePath = path.join(rootPath, './src');
const distPath = path.join(rootPath, './dist');

const VENDOR_FILE_NAME = 'vendor';

const vendors = ['lodash', 'react', 'react-dom', 'moment', 'material-ui', 'build-url', 'rest', 'react-tap-event-plugin'];

const plugins = [
	new CleanWebpackPlugin(distPath, { root: rootPath }),
	new webpack.optimize.CommonsChunkPlugin({
		name: VENDOR_FILE_NAME,
		minChunks: Infinity,
		filename: `${VENDOR_FILE_NAME}.bundle.js`
	}),
	new webpack.optimize.CommonsChunkPlugin({ name: 'manifest', minChunks: Infinity }),
	new webpack.NamedModulesPlugin(),
	new HtmlWebpackPlugin({
		template: path.resolve(rootPath, 'index.html')
	}),
];

module.exports = (env) => {
	const nodeEnv = env && env.prod ? ENV_PROD : ENV_DEV;
	const isProdEnv = nodeEnv === ENV_PROD;
	const isDevEnv = !isProdEnv;
	const config = isProdEnv ? prodConfig : devConfig;

	console.log(config.devtool);

	return {
		context: rootPath,
		devtool: config.devtool,
		entry: {
			index: path.resolve(sourcePath, 'index.js'),
			[VENDOR_FILE_NAME]: vendors,
		},
		output: {
			path: distPath,
			filename: '[name].[hash].js',
		},
		module: {
			rules: [{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			}],
		},
		resolve: {
			modules: [
				path.resolve(rootPath, 'node_modules'),
				path.resolve(sourcePath, 'components'),
			]
		},
		plugins: [
			new webpack.DefinePlugin({
				'process.env': { NODE_ENV: JSON.stringify(nodeEnv) },
				'isDevMode': JSON.stringify(isDevEnv),
				'API_ENV': JSON.stringify('server')
			})
		].concat(plugins, config.plugins),
		performance: config.performance,
		devServer: config.devServer,
	};
};