const webpack = require('webpack');

exports.devtool = 'source-map';

exports.plugins = [
	new webpack.LoaderOptionsPlugin({
		minimize: true,
		debug: false
	}),
	new webpack.optimize.UglifyJsPlugin({
		compress: {
			warnings: false,
			screw_ie8: true,
			conditionals: true,
			unused: true,
			comparisons: true,
			sequences: true,
			dead_code: true,
			evaluate: true,
			if_return: true,
			join_vars: true,
		},
		output: {
			comments: false,
		},
	})
];

exports.performance = {
	maxAssetSize: 100,
	maxEntrypointSize: 300,
	hints: 'warning',
};